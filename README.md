# Nano

Nano is a Ruby framework for making simple **[Discord](http://discordapp.com)**
bots. It is currently in development. Based on
[Discordrb](https://github.com/meew0/discordrb/), its goal is to simplify bot
development even more and bring bot development to the masses. I hope to make
this framework simple enough that people can learn how to code in Ruby or code
in general with its help by working on something that they enjoy.

Since Nano is still a work in progress, things may change and the overall
experience may be a bit rough around the edges. As it's still in development, it's
not intended for absolute newbies.

Nano is the most simple to install and is tested on Linux, as most (cheap) servers
are running some flavour of Linux. Please note that **Nano is not a plug and play
bot** - it's more like a set of Legos that you have to assemble, but you can
assemble them in a virtually unlimited amount of ways. In other words, if you
want something that *just works*, this project is not for you.

## Getting started with plugins

Nano's greatest strength is the plugin system. The plugins you write can be
shared and put in another Nano instance without any adjustments, and you can
download other people's plugins and use them in your bot. This lets you add and
remove functionality as you please, and makes every Nano bot different.

There are three ways to acquire plugins:

* Plugins that ship with Nano are located in the `plugins` directory. You can
  enable them as you wish in the configuration file. Eventually this directory
  will fill up with useful plugins instead of tech demos. **Please do not alter
  the contents of this directory** as it will make updating tough.
* Plugins that you code should be placed in the `plugins/user` directory. These
  will be ignored by the updater! You can also put the plugins that you download
  there.
* External plugins can be downloaded as gems. This is experimental and untested.

After you've downloaded or coded a plugin, you'll need to enable it in
`application.yml` (described under the "Installation" section)

## Current features

* Plugin system
* Simple configuration
* Moderately useful and simple permissions plugin
* Music!
* Noob-friendly plugin syntax

## Planned features

* An assortment of useful, general-purpose plugins
* Full-fledged customizable permission system based on Discord's
* Redis-powered persistent storage

## Installation

* If you haven't already, install [Ruby](http://ruby-lang.org) (for newbies, I
  recommend using [Ruby Version Manager](https://rvm.io/) as it will make your
  life a lot easier),
  [Bundler](http://bundler.io/) and (optionally) [Redis](http://redis.io/)
* Clone or download this Git project.
* Run `bundle install` in the project folder.
* Copy `config/example.application.yml` to `config/application.yml` and edit it
  as detailed in the file.
* Start hacking! Run `ruby run_bot.rb` to start the bot, and you can run
  `git pull` to update your bot - make sure to read the changelog if you do!

## License
```
MIT License

Copyright (c) 2016 Ivan Oštrić

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
