# Description:
#   Tutorial plugin.
#
# Commands:
#   nano hello - Says hi!
#
# Author:
#   crescentrose


# This is an example plugin that demonstrates basic plugin loading with Nano.
# Nano is, in essence, a simple wrapper around discordrb that enforces a
# structure to the commands, making it easier to share different commands and
# contribute to their development. It was inspired by Hubot, a GitHub-authored
# bot for various online chatrooms.
#
# In essence this means that, if someone wrote a command for, example, dice
# rolls, you no longer need to code your own bot and deal with its configuration
# or try to adjust their implementation into yours - they can just publish it on
# RubyGems and you can include it with a single command.
#
# Nano also has a permission system that ties in with Discord's (so you do not
# have to keep track of two separate permission sets), and utilizes a database
# for permanent storage.
class HelloWorld < Plugin

  def initialize()
    # This is where you can define anything not covered by the Nano framework,
    # or anything you want executed before the bot runs.
    # You must also register your commands here.
    command name: "hello", help: "Hello from the hello_world plugin!" do
      reply "Hi #{mention_user}!"
    end
  end

end
