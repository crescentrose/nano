# Description:
#   Displays help and performs other maintenance tasks.
#
# Commands:
#   nano help - See all commands.
#   nano restart - Restart the bot.
#
# Author:
#   crescentrose

class Help < Plugin
  def initialize()
    command name: "help", help: "Display a list of all commands.", arguments: [
      page: "The name of the plugin or the command you need help with."
    ], separator: nil, method: "help"

    command(name: "service", help: "Send a service message to the bot.", arguments: [message: "The message to send."], restricted: true) do |event, message|
      $maintenance_commands.push(message.to_sym)
      reply "Service message '#{message}' sent to the launcher."
    end

    command name: "about",  help: "Display data about the bot." do
      reply "Nano running on #{Nano.bot.profile.username}##{Nano.bot.profile.discriminator}
Code base version #{Nano.bot.name} from https://gitlab.com/crescentrose/nano"
    end

  end

private
  def help(event, query = nil)
    response = ""
    if query != nil && query != ""
      commands = Nano.commands.select { |k,pl| pl[:plugin].downcase == query.downcase }
      # we found a plugin, let's spit out what we know about it
      if commands.length > 0
        response << "**Available commands in plugin #{query}:**\n"
        commands.each do |name, command|
          if Permissions.is_available?(command, event)
            entry = "#{command[:name]}"
            if command[:arguments]
              command[:arguments].each do |arg|
                # ???
                arg.each do |k,v|
                  entry = "#{entry} [#{k.to_s}]"
                end
              end
            end
            entry = "#{entry} #{" " * [29 - entry.length, 1].max}#{command[:help]}"
            response = "#{response}\n#{entry}"
          end
        end
        reply response
      else
        command = Nano.commands.select { |k,cm| cm[:name].downcase == query.downcase }
        if command.length > 0
          command.each do |name, command|
            response << "**#{name}"
            if command[:arguments]
              command[:arguments].each do |arg|
                arg.each do |k,v|
                  response << " [#{k.to_s}]"
                end
              end
              response << "**\n#{command[:help]}\n"
              response << "\nArguments:\n"
              command[:arguments].each do |arg|
                arg.each do |k,v|
                  response << "*#{k.to_s}* - #{v}\n"
                end
              end
            end
            reply response
          end
        else
          reply "No command or plugin called `#{query}` found."
        end
      end
    else
      response << "**Available plugins:**\n"
      Nano.plugins.each do |pl|
        response << "#{pl.class.name}  "
      end
      response << "\n\nUse `nano help [plugin]` or `nano help [command]` for help with a plugin or a command."
      reply response
    end
  end
end
