# Description:
#   A simple dice rolling plugin
#
# Commands:
#   nano d6       - roll a D6
#   nano roll <n> - roll a <n>-sided dice
#
# Author:
#   crescentrose

class Dice < Plugin
  def initialize()
    command name: "d6", help: "Roll a D6.", method: "d6"
    command name: "roll", help: "Roll a custom dice", arguments: [sides: "Amount of sides the dice has"], method: "roll"
  end

  def d6(event)
    event.respond "#{event.user.name} rolled #{1+rand(6)}."
  end

  def roll(event, sides)
    event.respond "#{event.user.name} rolled #{1+rand(sides.to_i)}."
  end
end
