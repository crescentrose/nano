# Description:
#   Manages the default permission manager.
#
# Commands:
#   nano permission check command - Check if you can run the command
#   nano permission check_as @user "command" - Check if an user can run the command.
#   nano permission grant "command" to role "role" - Allow a specific command to be used by a role.
#   nano permission grant "command" to user @user  - Allow a specific command to be used by an user.
#   nano permission deny "command" from role "role" - Deny a specific command from being used by a role.
#   nano permission deny "command" from user @user - Deny a specific command from being used by an user.
#
# Author:
#   crescentrose

class Permission < Plugin
  def initialize()
    command name: "permission check", help: "Check whether you can access a command", arguments: [command: "Name of the command"], method: "check", separator: nil
    command name: "permission check_as", help: "Check whether an user can access a command", arguments: [user: "User tag", cmd: "Name of the command"], method: "check_as", restricted: true
    command name: "permission grant", help: "Grant a command to an user or a role.", method: "grant", restricted: true
    command name: "permission deny", help: "Deny a command from an user or a role.", method: "deny", restricted: true
  end

  private

  def check(event, command)
    if Permissions.is_available?(Nano.commands[command], event)
      reply "#{mention_user}, you are allowed to use this command on this server."
    elsif
      reply "#{mention_user}, you **don't have the permission** to use this command on this server :("
    end
  end

  def grant(event)
    if event.message.content =~ /permission grant (plugin)?.?"(.*)" to role "(.*)"/
      plugin = Regexp.last_match[1]
      command = Regexp.last_match[2]
      role = Regexp.last_match[3]

      if Nano.commands[command] || plugin == "plugin"
        # get role ID
        event.server.roles.each do |server_role|
          if role == "everyone"
            role = "@everyone"
          end
          if role == server_role.name
            if plugin == "plugin"
              Nano.commands.each do |name, cmd|
                if cmd[:plugin] == command
                  Permissions.grant_to_role(role: server_role.id, server: event.server.id, command: name)
                end
              end
              reply "Role #{role} can now use all commands in plugin #{command}."
            else
            Permissions.grant_to_role(role: server_role.id, server: event.server.id, command: command)
              reply "Role #{role} can now use #{command}."
            end
          end
        end
      else
        reply "That command does not exist."
      end

    elsif event.message.content =~ /permission grant (plugin)?.?"(.*)" to user/
      plugin = Regexp.last_match[1]
      command = Regexp.last_match[2]
      users = event.message.mentions

      if Nano.commands[command] || plugin == "plugin"
        users.each do |user|
          if plugin == "plugin"
            Nano.commands.each do |name, cmd|
              if cmd[:plugin] == command
                Permissions.grant_to_user(user: user.id, server: event.server.id, command: name)
              end
            end
            reply "User #{user.mention} can now use all commands in plugin #{command}."
          else
            Permissions.grant_to_user(user: user.id, server: event.server.id, command: command)
            reply "User #{user.mention} can now use #{command}."
          end

        end
      else
        reply "That command does not exist."
      end
    else
        reply "Usage: ```nano permission grant \"command\" to role \"role\" - Allow a specific command to be used by a role.
nano permission grant \"command\" to user @user  - Allow a specific command to be used by an user.
nano permission grant plugin \"plugin\" to role \"role\" - Allow all commands in a plugin to be used by a role.
nano permission grant plugin \"plugin\" to user @user  - Allow all commands in a plugin to be used by an user.```"
    end
  end

  def deny(event)
    if event.message.content =~ /permission deny (plugin)?.?"(.*)" from role "(.*)"/
      plugin = Regexp.last_match[1]
      command = Regexp.last_match[2]
      role = Regexp.last_match[3]

      if Nano.commands[command] || plugin == "plugin"
        # get role ID
        event.server.roles.each do |server_role|
          if role == "everyone"
            role = "@everyone"
          end
          if role == server_role.name
            if plugin == "plugin"
              Nano.commands.each do |name, cmd|
                if cmd[:plugin] == command
                  Permissions.deny_from_role(role: server_role.id, server: event.server.id, command: name)
                end
              end
              reply "Role #{role} can no longer use any command in plugin #{command}."
            else
            Permissions.deny_from_role(role: server_role.id, server: event.server.id, command: command)
              reply "Role #{role} can no longer use #{command}."
            end
          end
        end
      else
        reply "That command does not exist."
      end

    elsif event.message.content =~ /permission deny (plugin)?.?"(.*)" from user/
      plugin = Regexp.last_match[1]
      command = Regexp.last_match[2]
      users = event.message.mentions

      if Nano.commands[command] || plugin == "plugin"
        users.each do |user|
          if plugin == "plugin"
            Nano.commands.each do |name, cmd|
              if cmd[:plugin] == command
                Permissions.deny_from_user(user: user.id, server: event.server.id, command: name)
              end
            end
            reply "User #{user.mention} may not use any commands in plugin #{command}."
          else
            Permissions.deny_from_user(user: user.id, server: event.server.id, command: command)
            reply "User #{user.mention} may not use #{command}."
          end

        end
      else
        reply "That command does not exist."
      end
    else
        reply "Usage: ```nano permission deny \"command\" from role \"role\" - Disallow a specific command from being used by a role.
nano permission deny \"command\" from user @user  - Disallow a specific command from being used by an user.
nano permission deny plugin \"plugin\" from role \"role\" - Disallow all commands in a plugin from being used by a role.
nano permission deny plugin \"plugin\" from user @user  - Disallow all commands in a plugin from being used by an user.```"
    end
  end
end
