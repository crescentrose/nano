# Description:
#   Announces whenever an user joins and optionally gives them a role.
#   Announces whenever an user leaves, or another moderately important event occurs.
#
# Commands:
#   nano welcome role [role name] - Sets the default role for new users.
#   nano announce channel - Sets the current channel as the announcements channel.
#
# Providers:
#   announce(server_id, message)
#
# Author:
#   crescentrose

class Announce < Plugin
  def initialize()
    command name: "welcome role", help: "Set the default role for new users", arguments: [role_name: "Name of the role"], method: "change_default_role", restricted: true
    command name: "announce channel", help: "Set the current channel as a greeting channel for new users", method: "change_welcome_channel", restricted: true

    provides :announce, with: "announce"

    Nano.bot.member_join() do |event|
      welcome_user(event)
    end
    Nano.bot.member_leave() do |event|
      goodbye_user(event)
    end
  end

  private
  def announce(server, message)
    channel = Nano.brain.get("welcome_#{server}_channel")
    if channel != nil
      Nano.bot.send_message(channel, message)
    end
  end

  def goodbye_user(event)
    announce(event.server.id, "#{event.user.username}##{event.user.discriminator} has left the server.")
  end

  def welcome_user(event)
    announce(event.server.id, "#{event.user.mention} has joined the server!")
    welcome_role = Nano.brain.get("welcome_#{event.server.id}_role")
    if welcome_role != nil
      event.server.roles.each do |role|
        if welcome_role === role.name
          event.user.add_role(role)
        end
      end
    end
  end

  def change_welcome_channel(event)
    Nano.brain.set("welcome_#{event.server.id}_channel", event.channel.id)
    reply "This channel (#{event.channel.id}) will now be the welcome channel for this server."
  end

  def change_default_role(event, role)
    Nano.brain.set("welcome_#{event.server.id}_role", role)
    reply "Role #{role} will now be the welcome role for this server."
  end
end
