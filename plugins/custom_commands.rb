# Description:
#   Make simple commands.
#
# Commands:
#   nano add_command $command:$text - add a $command that returns $text
#
# Author:
#   crescentrose

class CustomCommands < Plugin
  def initialize()
    command name: "add_command", help: "Add a command that returns some text.",
      arguments: [command: "Command name", text: "Return value"], method: "add_command", restricted: true

    # load previous commands
    Nano.brain.hgetall("custom_commands").each do |command, text|
      on :message, content: /^#{Configuration.data["command_prefix"]}#{command}.*/i do |event|
        reply text
      end
    end
  end

  def add_command(event, command, *text)
    Nano.brain.hset("custom_commands", command, text.join(" "))

    # register a command for the current instance
    on :message, content: /^#{Configuration.data["command_prefix"]}#{command}.*/i do |event|
      reply text.join(" ")
    end

    reply "Command successfully added!"
  end
end
