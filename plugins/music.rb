# Description:
#   Play music on your server.
#
# Commands:
#   nano music join       - Joins the channel that the owner is currently in
#   nano music leave      - Leaves the channel
#   nano music remove <n> - Removes nth song from the queue, or all songs
#   nano music add <url>  - Adds a song from YouTube (any youtube-dl supported site works)
#   nano music queue      - See the queue
#   nano music play       - Start or resume playback
#   nano music pause      - Pause playback
#   nano music skip       - Skip the current song
#
# Providers:
#   music_play(event, url_or_search_string)
#
# Author:
#   crescentrose


require "youtube-dl"

class Music < Plugin
  include Filesystem

  def initialize()
    @is_joined = false
    @queue = Array.new()
    @voicebot = nil
    @now_playing = nil
    @paused = false

    command name: "music join", help: "Join the channel you are in", method: "join"
    command name: "music leave", help: "Leave this server's voice channel", method: "leave", restricted: true
    command name: "music remove", help: "Remove a song from the queue", arguments: [number: "The number of the song to remove"], method: "remove", restricted: true
    command name: "music add", help: "Add a YouTube video to the queue", arguments: [url: "Link to the video"], method: "add", separator: nil
    command name: "music queue", help: "Check out the music queue", method: "queue"
    command name: "music play", help: "Play the next song in the queue", method: "play"
    command name: "music pause", help: "Pause the playback", method: "pause"
    command name: "music skip", help: "Skip to the next song in the queue", method: "skip"

    provides :music_play, with: :add

    # ensure that storage exists
    @storage = storage :clean
  end

  private
  def join(event)
    # check if it was a PM
    if event.server == nil
      # not using reply since join may be called from add, which is a provider method
      event.respond "You can't use this in a PM."
      return
    end
    if @is_joined
      event.respond "Bot is already in a voice channel."
      return
    end

    begin
      # Join owner's channel
      @voicebot = Nano.bot.voice_connect(event.message.author.voice_channel)
      @is_joined = true
    rescue NoMethodError
      @is_joined = false
      event.respond "You need to be in a voice channel to use this."
      return
    end

    # this should be fetched from config/redis in the future
    @voicebot.volume = 0.35
    @voicebot.length_override = Configuration.data["voice_packet_length"] if Configuration.exists "voice_packet_length"
  end

  def leave(event)
    @voicebot.destroy
    @is_joined = false
    Nano.bot.game = nil
    @now_playing = nil
  end

  def add(event, url)
    # download the song, extract the MP3, push it to the queue
    # youtubedl gem just uses ruby-fied command line parameters as options
    song = YoutubeDL.download url, extract_audio: true, output: "#{@storage}/%(title)s.mp3", audio_format: "mp3", default_search: "ytsearch"
    data = {title: song.information[:title], filename: song.filename, added_by: event.user.name}
    @queue.push(data)
    event.respond "Enqueued **#{data[:title]}** for playback."
    if @is_joined != true
      join(event)
    end
    if @now_playing == nil && @is_joined == true
      play(event)
    end
  end

  def queue(event)
    # just format my shit up fam
    if @now_playing
      response = "Now playing: **#{@now_playing[:title]}** added by #{@now_playing[:added_by]}"
    else
      response = "Now playing: (nothing)"
    end
    response = "#{response}\nCurrent queue: \n"
    i = 1
    @queue.each do |song|
      response = "#{response}#{i}. **#{song[:title]}** added by #{song[:added_by]}\n"
      i = i + 1
    end
    reply response
  end

  def play(event)
    # this is used both to start and resume playback
    if @paused == true
      @paused == false
      @voicebot.continue
      Nano.bot.game = "♬ #{@now_playing[:title]}"
      return
    end
    # remove the song from the start of the queue, puhpuhpuhpUHPUHPLAY IT
    begin
      song = @queue.shift
      @now_playing = song
      event.respond "Now playing: **#{song[:title]}** added by #{song[:added_by]}"
      Nano.bot.game = "♬ #{song[:title]}"
      @voicebot.play_file(song[:filename])
    end until @queue.length == 0
    @now_playing = nil
    Nano.bot.game = nil
    event.respond "Playlist ended!"
  end

  def pause(event)
    @voicebot.pause
    @paused = true
    Nano.bot.game = "♬ [paused] #{@now_playing[:title]}"
  end

  def skip(event)
    # this terminates the currently playing song, but since the play event is
    # in a loop, it immediately skips to the next song
    @voicebot.stop_playing
  end

  def remove(event, number)
    if number == "all"
      @queue = []
    else
      i = number.to_i - 1
      if i == -1
        reply "Usage: `nano music remove <number>` or `nano music remove all`"
      else
        @queue.delete_at(i)
      end
    end
  end
end
