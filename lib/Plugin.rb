class Plugin
  include Command
  include Responses

  attr_accessor :current_event

  def self.list_plugins
    ObjectSpace.each_object(Class).select { |klass| klass < self }
  end

  def initialize()
    puts "[INFO] #{self.class.name} has no constructor, this might or might not be what you want."
  end

  def provides(service, with:)
    Provider.add_provider(service, self.method(with.to_sym), self)
  end

  ##
  # A wrapper around the discordrb events
  # Enables the use of convenience methods such as 'reply' and 'reply_with_file'
  def on(event, params)
    Nano.bot.send(event.to_s, params) do |event|
      # some discordrb fuckery causes running the bot with debug logging to not
      # run??? and running the bot with normal logging to have exceptions not
      # output anything??? so this is a "workaround"
      begin
        @current_event = event
        yield event
        @current_event = nil
      rescue Exception => e
        handle_exception(e, event.message)
      end
    end
  end
end
