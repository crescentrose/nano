# This module interacts with the Redis database to allow for simple key-value
# storage.
module Brain
  def Brain.connect()
    # TODO: add password: Configuration.data["redis_password"]
    @redis = Redis.new(:host => Configuration.data["redis_host"], :port => Configuration.data["redis_port"],
     :db => Configuration.data["redis_db"])
    return @redis
  end

  def Brain.instance()
    @redis
  end

  # TODO: Add more helper methods
end
