##
# This module provides the ability to store data on the file system.
# You can use this for storing temporary files, such as audio files downloaded
# by the music bot, or persistent files, such as images.

module Filesystem
  ##
  # Initialize a storage system for the plugin. This will create necessary
  # directories under `/storage` if they don't exist. Pass `:clean` as a
  # parameter to clear the directory.
  def storage (clean = :persistent)
    name = self.class.name.downcase
    dirname = File.dirname("./storage/#{name}")
    FileUtils.mkdir_p(dirname) unless File.directory?(dirname)
    if clean == :clean
      FileUtils.rm_rf(Dir.glob("./storage/#{name}/"))
    end
    return "./storage/#{name}/"
  end
end
