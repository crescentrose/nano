module Responses
  ##
  # Actions
  def reply(msg)
    @current_event.respond(msg)
  end

  def reply_with_file(file)
    @current_event.channel.send_file file
  end

  ##
  # Helper methods
  def mention_user
    @current_event.user.mention
  end

  def user
    @current_event.user.name
  end
end
