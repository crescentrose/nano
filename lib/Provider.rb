module Provider
  ##
  # Register a provider for a specific action.
  # Providers make communication between plugins easier. For example, if you
  # were writing a moderation bot, you might want to have a chanel for logging
  # actions, which is already implemented in the Announce standard plugin.
  # This lets you leverage existing code and configuration without having to
  # write your own logging implementation.
  # You can register any method as a provider.
  def self.add_provider(service, method, instance)
    puts "[INFO] Registering provider #{service} to #{method}."
    if Nano.providers[service]
      Nano.providers[service].push ({method: method, instance: instance})
    else
      Nano.providers[service] = [{method: method, instance: instance}]
    end
  end

  def self.method_missing(method, *args, &block)
    if Nano.providers[method.to_sym] != nil
      Nano.providers[method.to_sym].each do |provider|
          return provider[:method].call(*args)
      end
    else
      puts "[WARN] No provider for #{method} found."
      puts "[WARN] #{caller.join("\n[WARN] ")}"
    end
  end
end
