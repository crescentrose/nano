require "digest"

module Permissions
  def Permissions.is_available?(command, event)
    brain = Brain.instance()
    command_hash = Digest::MD5.digest command[:name]

    # owner permission overrides everything
    if event.author.id == Configuration.data["discord_owner_id"].to_i
      return true
    end

    # user permissions override role permissions
    user_permission = brain.hget("permissions:#{event.server.id}:user:#{event.author.id}", command_hash)
    if user_permission == "deny"
      return false
    elsif user_permission == "allow"
      return true
    end

    # role permissions are the weakest
    event.author.roles.each do |role|
      role_permission = brain.hget("permissions:#{event.server.id}:role:#{role.id}", command_hash)
      if role_permission == "deny"
        return false
      elsif role_permission == "allow"
        return true
      end
    end

    if command[:restricted]
      # if no explicit permission was granted for a restricted command, default to deny
      return false
    else
      # if a public command wasn't explicitly denied, allow it
      return true
    end
  end

  def Permissions.grant_to_role(role:, server:, command:)
    Brain.instance().hset("permissions:#{server}:role:#{role}", Digest::MD5.digest(command), "allow")
  end

  def Permissions.grant_to_user(user:, server:, command:)
    Brain.instance().hset("permissions:#{server}:user:#{user}", Digest::MD5.digest(command), "allow")
  end

  def Permissions.deny_from_role(role:, server:, command:)
    Brain.instance().hset("permissions:#{server}:role:#{role}", Digest::MD5.digest(command), "deny")
  end

  def Permissions.deny_from_user(user:, server:, command:)
    Brain.instance().hset("permissions:#{server}:user:#{user}", Digest::MD5.digest(command), "deny")
  end
end
