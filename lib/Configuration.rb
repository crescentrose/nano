module Configuration
  def Configuration.load
    if !File.exists?('./config/application.yml')
      puts "[ERROR] Can't find the configuration file."
      puts "[ERROR] Make sure config/application.yml exists!"
    end

    @config = begin
      YAML.load_file('./config/application.yml')
    rescue ArgumentError => e
      puts "[ERROR] Can't parse YAML."
      puts "[ERROR] #{e.message}"
      exit
    end
  end

  def Configuration.data
    @config
  end

  def Configuration.exists key
    if @config[key]
      return true
    else
      return false
    end
  end
end
