module Nano

  def self.initialize
    # Load the bot
    @@bot = Discordrb::Bot.new token: Configuration.data["discord_token"], client_id: Configuration.data["discord_client_id"]
    @@bot.name = "Nano v0.2"
    # @@on_load = Array.new()
    # Make a commands list
    @@commands = Hash.new()
    # Load Redis
    @@brain = Brain.connect()
    # Load the plugins

    if Configuration.data["plugins"]
      Configuration.data["plugins"].each do |plugin|
        require "./plugins/#{plugin}"
        puts "[INFO] Loaded #{plugin} plugin."
      end
    end

    if Configuration.data["user_plugins"]
      Configuration.data["user_plugins"].each do |plug|
        require "./plugins/user/#{plug}"
        puts "[INFO] Loaded #{plug} plugin."
      end
    end

    if Configuration.data["external_plugins"]
      Configuration.data["external_plugins"].each do |plugin|
        require "#{plugin}"
        puts "[INFO] Loaded #{plugin} plugin."
      end
    end
    @@plugins = Array.new()
    @@providers = Hash.new()

    # Load every plugin
    Plugin.list_plugins.each do |plugin|
      pl = plugin.new()
      puts "[INFO] Initialized #{pl.class.name}"
      @@plugins.push pl
    end

    self.setup_heartbeats(60)

    # Execute the on_load code, if any.
    #@@on_load.each do |block|
    #  block.call
    #end

  end

  def self.run
    @@bot.run :async
  end

  def self.stop
    @@bot.stop
  end

  def self.bot
    @@bot
  end

  def self.commands
    @@commands
  end

  def self.brain
    @@brain
  end

  def self.plugins
    @@plugins
  end

  def self.providers
    @@providers
  end

  def self.setup_heartbeats(interval)
    return if @heartbeat_thread
    @heartbeat_interval = interval
      @heartbeat_thread = Thread.new do
      Thread.current[:discordrb_name] = 'heartbeat'
      loop do
        begin
          if (@session && !@session.suspended?) || !@session
            sleep @heartbeat_interval
            @@bot.raise_heartbeat_event
          else
            sleep 1
          end
        rescue => e
          puts "Error while heartbeating. FeelsBadMan"
          puts e.message
        end
      end
    end
  end
end
