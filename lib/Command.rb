module Command

  ##
  # Define a command.
  # Commands must be defined in constructor.
  def command(name:, help:, method: nil, arguments: nil, separator: " ", restricted: false, &block)
    # The data on the command is put into an array for help generation.
    cmd = {
      name: name,
      help: "[#{self.class.name}] #{help}",
      plugin: self.class.name,
      arguments: arguments,
      restricted: restricted
    }

    # Make sure we aren't defining an existing command.
    Nano.commands.each do |cmdname, cmnd|
      if (cmdname == name)
        # TODO: replace with exception
        puts "[ERROR] Plugin #{self.class.name} is trying to define #{cmnd[:name]}, which is already defined in #{cmnd[:plugin]}."
        puts "[ERROR] The new definition will be ignored!"
        return
      end
    end

    # Add the command to the command list.
    Nano.commands[cmd[:name]] = cmd

    # Register the event.
    on :message, content: /^#{Configuration.data["command_prefix"]}#{name}.*/i do |event|
      # Ask the Permissions module if we can use the command.
      if !Permissions.is_available?(cmd, event)
        reply "You can not use this command."
        return
      end

      # Pluck out the arguments.
      t = event.message.text.gsub(/#{Configuration.data["command_prefix"]}#{name}\s?/i, "")
      if arguments != nil
        if separator == nil
          args = [event, t]
        else
          args = [event, *(t.split(separator))]
        end
      else
        args = [event]
      end

      # Check if we have a block, and if we do run it with the proper arguments.
      # Otherwise, run the method that we were given.
      if block_given?
        case block.arity
          when 0
            yield
          when 1
            yield event
          else
            yield *args
        end
      else
          self.send(method, *args)
      end
    end
  end

  ##
  # Define a command that will be executed even if someone doesn't directly invoke the bot, based on a regex or a string.
  # These won't be checked for duplicate commands or put in the help file.
  def hear(regex, method: nil, &block)
    on :message, contains: regex do |event|
      @current_event = event
      if block_given?
        case block.arity
          when 0
            yield
          when 1
            yield event
        end
      else
        begin
          self.send(method)
        rescue Exception => e
          handle_exception(e, "(hear) #{regex}")
        end
      end
    end
  end

  def handle_exception(e, name = "(unknown)")
    puts "[ERROR] Exception while handling #{name}."
    puts "[ERROR] #{e.message}"
    puts "[ERROR] #{e.backtrace.join("\n[ERROR] ")}"
  end
end
