#!/bin/env ruby
require './config/config'
$maintenance_commands = Queue.new

Nano.initialize
Nano.run
puts "Nano is running. Hit ENTER to exit."

maintenance = Thread.new do
  while true do
    command = $maintenance_commands.pop
    case command
    when :restart
      puts "[INFO] Attempting to restart the bot."
      Nano.stop
      puts "[INFO] Nano stopped, will launch #{Gem.ruby} #{__FILE__} next."
      Kernel.exec("#{Gem.ruby} #{__FILE__}")
    when :update
      puts "[INFO] Attempting git pull..."
      `git pull`
      puts "[INFO] git pull says #{$?}."
      puts "[INFO] Queuing a restart..."
      $maintenance_commands.push :restart
    when :shutdown
      puts "[INFO] Bot shutting down."
      Nano.stop
      exit
    when :ping
      puts "[INFO] Pong!"
    end
  end
end

gets

Nano.stop
