# load the dependencies

# setup bundler
require 'rubygems'
require 'bundler/setup'

# on windows, we need to load libsodium and inject the /bin directory into
# PATH so that Discordrb can load ffmpeg, ffprobe and opus
if Gem.win_platform?
  puts "Plebian OS detected, preloading libsodium.dll and opus.dll"

  puts "Note: for voice sending to work, you'll have to install DevKit and then"
  puts "reinstall the discordrb gem. Instructions for this should be on"
  puts "the bot download page."

  # enhance PATH to load ffmpeg, ffprobe and opus
  ENV["PATH"] = "#{ENV["PATH"]};#{File.expand_path("..", __dir__)}/bin"
  # manually load libsodium
  ::RBNACL_LIBSODIUM_GEM_LIB_PATH = "bin/libsodium.dll"
end

# setup gems
require 'discordrb'
require 'json'
require 'yaml'
require 'redis'
require 'fileutils'
require 'require_all'
require 'thread'

require './lib/Nano.rb'
# setup local libs
require_all 'lib'

# load the config data
Configuration.load
