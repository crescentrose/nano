## Providers

Nano lets you *share code across plugins* by using the Provider middleman class.
Providers make it easy to leverage the functionality of existing plugins in your
own. For example - if you have the defautl plugin for music installed, and want
to queue in a song from another plugin, you don't have to re-code the music
feature - just call the `music_play` provider and the framework will do all the
work for you.

Likewise, if you believe a feature you wrote might be useful to other plugins,
you can register a provider by simply adding the `provides` method to your
plugin's constructor.

If there is more than one provider for a specific action (for example, two
music plugins), the relevant action will be broadcast to all providers.

### Using providers

We'll demonstrate providers on the example of a provider in the default Announce
plugin. Announce plugin has an option of setting up an announcement channel
(one per server) for announcing whenever a member joins or leaves the server. We
will leverage that function by expanding on our plugin example and logging when
any user uses the `ask` command.

```ruby
# Existing code
class MagicEightBall < Plugin
  def initialize()
    # ... the @answers array is defined here ...
    command name: "ask", help: "Ask oracle Nano a yes or no question." do
      reply "#{mention_user}, your answer is: #{@answers.sample}"
    end
  end
end
```

Any provider is accessible by calling its name on the `Provider` module. By
looking at `announce.rb`'s header, we can see that it provides an
`announce(server_id, message)` provider.

To find out the server ID, we'll need the `event` that triggered the response.
Luckily, Nano is smart enough to adapt - it will pass the `event` as a block
parameter automatically.

From there on, it's smooth sailing: we'll just add the `Provider.announce`
method to our `ask` command:

```ruby
command name: "ask", help: "Ask oracle Nano a yes or no question." do |event|
  reply "#{mention_user}, your answer is: #{@answers.sample}"
  Provider.announce(event.server.id, "#{mention_user} used command `ask`.")
end
```

### Making your own providers

Let's say that your `ask` plugin is so good, you want to let others use the same
functionality as a part of their plugins.

To do that, you'll first need to move the main functionality to a separate method.
Don't worry, that will be easy. Just remember to rework the `command` method in
the constructor to make use of the new method.

```ruby
private
def random_answer()
  return @answers.sample
end
```

*Tip: Since providers are meant to be agnostic to the current event, you can't use
helper methods like `reply` or `mention_user` in them. Use `event` methods and
properties instead.*

After that, you'll need to register the provider in the `initialize()` method of
your plugin:

```ruby
provides :random_answer, with: "random_answer"
```

The `provides` helper function takes two parameters: the first one is the name
of the provider (as a symbol) that other plugins will be invoking, and the second
one is the name of the method that should be invoked.

That's it - you can now call `Provider.random_answer` from *any* plugin, and Nano
will automatically route the request to your plugin.

### Complete code
```ruby
class MagicEightBall < Plugin
  def initialize()
    @answers = [
      "It is certain", "It is decidedly so", "Without a doubt", "Yes, definitely",
      "You may rely on it", "As I see it, yes", "Most likely", "Outlook good",
      "Yes", "Signs point to yes", "Reply hazy try again", "Ask again later",
      "Better not tell you now", "Cannot predict now", "Concentrate and ask again",
      "Don't count on it", "My reply is no", "My sources say no", "Outlook not so good",
      "Very doubtful"
    ]
    command name: "ask", help: "Ask oracle Nano a yes or no question." do
      reply "#{mention_user}, your answer is: #{random_answer()}"
    end

    provides :random_answer, with: "random_answer"
  end

  private
  def random_answer()
    return @answers.sample
  end
end
```
