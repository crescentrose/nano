As stated in the readme, the greatest strength of Nano is its plugin system. This wiki page will walk you through creating a basic plugin.

Note that Nano is very early in development and that the documentation might be incomplete, nonexistent or flat out wrong.

For this exercise, we're going to make a small bot that gives a random answer to the question asked. If you've ever had a magic 8 ball, well, that's it.

## Setting up

First off, make a new file called `magic_eight_ball.rb` in the `plugins/user` directory. After that, add the `magic_eight_ball` entry to the `user_plugins` list in the `config/application.yml` file.

![Example user_plugins in application.yml](http://puu.sh/q1oYA/3512cf6b86.png)

## A blank slate

All plugins in Nano are child classes of the Plugin class. If you don't know what that means, don't worry - just know that your plugin will be automatically loaded as long as you include it in the configuration file.

Put this in your newly created `magic_eight_ball.rb` file:

```ruby
class MagicEightBall < Plugin
  def initialize()
    # We're going to put our plugin code here.
  end
end
```

This creates a `MagicEightBall` class, and when Nano boots up it will run whatever's in the `initialize` method. This lets us define whatever behaviour we want.

## Adding our answers

Our magic eight ball plugin will first require us to define a list of possible answers. I'm going to use this [Wikipedia article](https://en.wikipedia.org/wiki/Magic_8-Ball#Possible_answers) to get the original Magic 8-ball's answers, however you can switch them out and change them as you wish.

We're going to use a list to store our answers. You can also load the answers from the internet, a local file or a database, but that's a bit too advanced for this tutorial.

Just add the following code to the `initialize()` method:

```ruby
    @answers = [
      "It is certain", "It is decidedly so", "Without a doubt", "Yes, definitely",
      "You may rely on it", "As I see it, yes", "Most likely", "Outlook good",
      "Yes", "Signs point to yes", "Reply hazy try again", "Ask again later",
      "Better not tell you now", "Cannot predict now", "Concentrate and ask again",
      "Don't count on it", "My reply is no", "My sources say no", "Outlook not so good",
      "Very doubtful"
    ]
```

## Registering the command

This list by itself won't actually do anything. We need to let Nano know what should happen when the user asks a question. For this, we're going to use a convenient method by the name of `command`. This method will register your command, let you attach code to it, add it to the help file and provide you with several ways to reply to your users. However, its usage is rather simple. Just add this under your answers list in the `initialize` method:

```ruby
    command name: "ask", help: "Ask oracle Nano a yes or no question." do
      # Code for our command goes here
    end
```

Feel free to change the name and the help text, but keep in mind that both of those have to be there.

## Adding the code

Finally, we need to respond to our user. For that, Nano provides a handy `reply` method. In addition, we're going to use Ruby's `sample` method to pick a random answer from our list of answers. The `command` block should now look like this:

```ruby
    command name: "ask", help: "Ask oracle Nano a yes or no question." do
      reply @answers.sample
    end
```

That's it! You can now boot up Nano and ask her whatever you want:

![Example answer](http://puu.sh/q1pG8/c8936313e3.png)

The new command is also automatically put in the help file (provided you enabled the `help` plugin):

![Example help](http://puu.sh/q1pXo/a5c9960e40.png)

## Going further

There's a lot more than Nano can do, but for this bonus section I'm going to focus on one of them. In some cases, a lot of users could be asking questions at once. To prevent confusion, we're going to mention the user whose answer we're displaying.

We're going to use Ruby's [string interpolation](https://rubymonk.com/learning/books/1-ruby-primer/chapters/5-strings/lessons/31-string-basics) to keep our code simple and readable, and Nano's `mention_user` method will make it insanely simple to do what we want to do.

Our `reply` command can now look like this:

```ruby
    reply "#{mention_user}, your answer is: #{@answers.sample}"
```

![New feature in action](http://puu.sh/q1qg3/a3fb6133d9.png)

Make sure to restart the bot after making code changes!

## Final plugin code

```ruby
class MagicEightBall < Plugin
  def initialize()
    @answers = [
      "It is certain", "It is decidedly so", "Without a doubt", "Yes, definitely",
      "You may rely on it", "As I see it, yes", "Most likely", "Outlook good",
      "Yes", "Signs point to yes", "Reply hazy try again", "Ask again later",
      "Better not tell you now", "Cannot predict now", "Concentrate and ask again",
      "Don't count on it", "My reply is no", "My sources say no", "Outlook not so good",
      "Very doubtful"
    ]

    command name: "ask", help: "Ask oracle Nano a yes or no question." do
      reply "#{mention_user}, your answer is: #{@answers.sample}"
    end

  end
end
```
