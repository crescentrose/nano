## Conventions for plugins

Please follow these conventions when coding plugins.

### Plugin names and locations
Your plugins should be put in the `plugins/user` directory. This directory is
ignored when updating Nano, so your plugins will not be broken.

You should not change the default plugins unless you're contributing to the main
repository. If you want to mess with them, disable them in the configuration and
copy them to the user directory, and change them there.

The plugin file name should be in snake_case. The plugin class name should follow
standard Ruby conventions.

### Plugin headers
Your plugin *must* have a header that follows this template:
```ruby
# Description:
#   A short description of what your plugin does.
#
# Commands:
#   nano command_1              - A command that does something useful.
#   nano command_2 [parameter]  - A command that does something fun.
#
# Providers:
#   provider(parameter1, parameter2)
#
# Author:
#   Your name, Github-Gitlab-Bitbucket link
```

You may include other data in the header if necessary.

### Plugin methods
Your plugin *should* include a constructor (an `initialize()` method) in which
you register commands, providers and other events. If there's no constructor,
Nano will be a bit upset, but will continue loading, however your plugin will be
useless.

Any other method in your plugin *should* be private. Use standardised providers
to communicate with other plugins: providers are implementation-agnostic. That
means your plugins will work even if the user replaces a plugin with another one
that registers the same provider, and the method will fail gracefully if no
providers can be found.

### Using maintenance commands
The bot launcher implements a `$maintenance_commands` queue as a global variable.
This lets plugins shut down, restart or update the bot, which are potentially
destructive actions. Please push commands to that queue only with restricted
commands and when the user knows what is going on.
